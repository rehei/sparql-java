Introduction 
=======

sparql-java supports the programmer when writing SPARQL queries in java. 

Limitations
=======

Only SELECT-Queries are supported. Feel free to fork. 

Example 
=======

This example is formated in the default Eclipse format style.

```java
public class Main {

	public static void main(String[] args) {

		String shortQuery = Q.prefix("books", "http://example.org/books/")
				.prefix("rdf", "http://example.org/rdf")
				.select("?book ?authorName", new where() {
					{
						$("?book books:author ?author");
						$("?author books:authorName ?authorName");
					}
				}).get();
		System.out.println(shortQuery);

		String longQuery = Q
				.base("http://example.org/someWorldOntology#")
				.prefix("rdf",
						"http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
				.prefix("owl", "http://www.w3.org/2002/07/owl#")
				.selectDistinct("?bookTitle ?authorName ?totalAmountOfBooks")
				.from("http://example.org/books/fantasy")
				.fromNamed("http://example.org/people", new where() {
					{
						select("SUM(?book) as ?totalAmountOfBooks",
								new where() {
									{
										$(new group() {
											{
												$("?book rdf:type <Book>");
											}
										}).union(new group() {
											{
												$("?book rdf:type <Magazine>");
											}
										});
									}
								});

						$(new group() {
							{
								$("?author <hasWritten> ?book");
							}
						}).union(new group() {
							{
								$("?author <hasContributedTo> ?book ");
							}
						});

						$("GRAPH <http://example.org/people> ?author <name> ?authorName");
						$("GRAPH <http://example.org/people> ?author <age> ?age");
						filter("?a > 20");
					}
				}).orderByDescending("?authorName").offset(10).limit(2).get();
		System.out.println(longQuery);
	}
}
```
