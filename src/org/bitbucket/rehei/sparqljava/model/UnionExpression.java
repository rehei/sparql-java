package org.bitbucket.rehei.sparqljava.model;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.rehei.sparqljava.expression.IVisitor;
import org.bitbucket.rehei.sparqljava.query.group;

public class UnionExpression implements IVisitable {

	public List<group> groups = new ArrayList<group>(); 
	
	public UnionExpression(group group) {
		this.groups.add(group);
	}
	
	public UnionExpression union(group group) {
		this.groups.add(group);
		return this;
	}

	@Override
	public <T> T accept(IVisitor<T> visitor) {
		return visitor.visit(this); 
	}

}
