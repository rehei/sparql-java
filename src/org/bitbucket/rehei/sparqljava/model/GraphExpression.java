package org.bitbucket.rehei.sparqljava.model;

import org.bitbucket.rehei.sparqljava.expression.IVisitor;


public class GraphExpression implements IVisitable {

	public enum GraphExpressionType {
		FILTER, OPTIONAL
	}

	public String expression;
	public GraphExpressionType type;
	
	public GraphExpression(GraphExpressionType type, String expression) {
		this.type = type;
		this.expression = expression;
	}

	@Override
	public <T> T accept(IVisitor<T> visitor) {
		return visitor.visit(this);
	}
	
}
