package org.bitbucket.rehei.sparqljava.model;

import org.bitbucket.rehei.sparqljava.expression.IVisitor;


public interface IVisitable {

	public <T> T accept(IVisitor<T> visitor);
	
}
