package org.bitbucket.rehei.sparqljava.model;

import org.bitbucket.rehei.sparqljava.expression.IVisitor;
import org.bitbucket.rehei.sparqljava.query.where;

public class SubSelect extends Select {

	public SubSelect(SelectType type, String projection, where where) {
		super(type, projection, where);
	}

	@Override
	public <T> T accept(IVisitor<T> visitor) {
		return visitor.visit(this);
	}
	
}
