package org.bitbucket.rehei.sparqljava.model;

import org.bitbucket.rehei.sparqljava.expression.IVisitor;


public class IntegerExpression implements IVisitable {

	public enum IntegerExpressionType {
		LIMIT, 
		OFFSET
	}

	public IntegerExpressionType type;
	public int param;
	
	public IntegerExpression(IntegerExpressionType type, int param) {
		this.type = type;
		this.param = param;
	}

	@Override
	public <T> T accept(IVisitor<T> visitor) {
		return visitor.visit(this);
	}
	
}
