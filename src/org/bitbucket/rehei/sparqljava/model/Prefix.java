package org.bitbucket.rehei.sparqljava.model;

import org.bitbucket.rehei.sparqljava.expression.IVisitor;

public class Prefix implements IVisitable {

	public String prefix;
	public String iri;

	public Prefix(String prefix, String iri) {
		this.prefix = prefix;
		this.iri = iri;
	}

	@Override
	public <T> T accept(IVisitor<T> visitor) {
		return visitor.visit(this);
	}

}
