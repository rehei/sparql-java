package org.bitbucket.rehei.sparqljava.model;

import org.bitbucket.rehei.sparqljava.expression.IVisitor;
import org.bitbucket.rehei.sparqljava.query.where;


public class From implements IVisitable {

	public enum FromType {
		NAMED, DEFAULT
	}
	
	public String iri;
	public FromType type;
	public where where;

	public From(FromType type, String iri, where where) {
		this.type = type;
		this.iri = iri;
		this.where = where;
	}

	@Override
	public <T> T accept(IVisitor<T> visitor) {
		return visitor.visit(this);
	}

}
