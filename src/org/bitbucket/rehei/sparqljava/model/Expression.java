package org.bitbucket.rehei.sparqljava.model;

import org.bitbucket.rehei.sparqljava.expression.IVisitor;


public class Expression implements IVisitable {

	public enum ExpressionType {
		GROUP_BY, HAVING, ORDER_BY_ASC, ORDER_BY_DESC
	}

	public String expression;
	public ExpressionType type;

	public Expression(ExpressionType type, String expression) {
		this.type = type;
		this.expression = expression;
	}

	@Override
	public <T> T accept(IVisitor<T> visitor) {
		return visitor.visit(this); 
	}
	
}
