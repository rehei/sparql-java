package org.bitbucket.rehei.sparqljava.model;

import org.bitbucket.rehei.sparqljava.expression.IVisitor;
import org.bitbucket.rehei.sparqljava.query.where;


public class Select implements IVisitable {

	public static enum SelectType {
		REDUCED, DISTINCT, DEFAULT
	}

	public String projection;
	public SelectType type;
	public where where;
	
	public Select(SelectType type, String projection, where where) {
		this.type = type;
		this.projection = projection;
		this.where = where;
	}

	@Override
	public <T> T accept(IVisitor<T> visitor) {
		return visitor.visit(this);
	}

}
