package org.bitbucket.rehei.sparqljava.model;

import org.bitbucket.rehei.sparqljava.expression.IVisitor;

public class SimpleExpression implements IVisitable {

	public String expression;

	public SimpleExpression(String expression) {
		this.expression = expression;
	}

	@Override
	public <T> T accept(IVisitor<T> visitor) {
		return visitor.visit(this);
	}

}
