package org.bitbucket.rehei.sparqljava.model;

import org.bitbucket.rehei.sparqljava.expression.IVisitor;

public class Base implements IVisitable {

	public String iri;

	public Base(String url) {
		this.iri = url;
	}

	@Override
	public <T> T accept(IVisitor<T> visitor) {
		return visitor.visit(this);
	}

}
