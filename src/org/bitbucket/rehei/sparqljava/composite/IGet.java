package org.bitbucket.rehei.sparqljava.composite;

import org.bitbucket.rehei.sparqljava.expression.IVisitor;

public interface IGet {

	public String get();
	
	public <T> T get(IVisitor<T> visitor);

}
