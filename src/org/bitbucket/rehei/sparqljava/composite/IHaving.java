package org.bitbucket.rehei.sparqljava.composite;

public interface IHaving {

	public IOrderOffsetLimitGet having(String str);
	
}
