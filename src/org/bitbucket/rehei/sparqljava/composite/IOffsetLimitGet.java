package org.bitbucket.rehei.sparqljava.composite;

public interface IOffsetLimitGet extends IOffset, ILimit, IGet {

}
