package org.bitbucket.rehei.sparqljava.composite;

public interface IGroupingHavingOrderOffsetLimitGet extends IGrouping, IHaving,
		IOrder, IOffset, ILimit, IGet {

}
