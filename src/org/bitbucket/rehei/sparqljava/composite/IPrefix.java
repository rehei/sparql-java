package org.bitbucket.rehei.sparqljava.composite;

public interface IPrefix {

	public IPrefixSelect prefix(String prefix, String url);

}