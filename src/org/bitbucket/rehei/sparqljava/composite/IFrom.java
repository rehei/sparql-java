package org.bitbucket.rehei.sparqljava.composite;

import org.bitbucket.rehei.sparqljava.query.where;

public interface IFrom {

	public IGroupingHavingOrderOffsetLimitGet from(String uri, where where);

	public IGroupingHavingOrderOffsetLimitGet fromNamed(String uri, where where);

	public IFromGroupingHavingOrderOffsetLimitGet from(String uri);

	public IFromGroupingHavingOrderOffsetLimitGet fromNamed(String uri);

}
