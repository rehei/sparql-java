package org.bitbucket.rehei.sparqljava.composite;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.rehei.sparqljava.expression.IVisitor;
import org.bitbucket.rehei.sparqljava.expression.StringVisitor;
import org.bitbucket.rehei.sparqljava.model.Base;
import org.bitbucket.rehei.sparqljava.model.Expression;
import org.bitbucket.rehei.sparqljava.model.From;
import org.bitbucket.rehei.sparqljava.model.IVisitable;
import org.bitbucket.rehei.sparqljava.model.IntegerExpression;
import org.bitbucket.rehei.sparqljava.model.Prefix;
import org.bitbucket.rehei.sparqljava.model.Select;
import org.bitbucket.rehei.sparqljava.model.Expression.ExpressionType;
import org.bitbucket.rehei.sparqljava.model.From.FromType;
import org.bitbucket.rehei.sparqljava.model.IntegerExpression.IntegerExpressionType;
import org.bitbucket.rehei.sparqljava.model.Select.SelectType;
import org.bitbucket.rehei.sparqljava.query.where;

public class QueryComposite implements IBasePrefixSelect, IBase, IPrefix,
		IPrefixSelect, ISelect, IFrom, IFromGroupingHavingOrderOffsetLimitGet,
		IGrouping, IGroupingHavingOrderOffsetLimitGet, IHaving,
		IHavingOrderOffsetLimitGet, IOrder, IOrderOffsetLimitGet, IOffset,
		IOffsetLimitGet, ILimit, ILimitGet, IGet {

	public List<IVisitable> list = new ArrayList<IVisitable>();

	public String get() {
		return this.get(new StringVisitor());
	}
	
	@Override
	public <T> T get(IVisitor<T> visitor) {
		return visitor.visit(list);
	}

	@SuppressWarnings("unchecked")
	public <T> T compose(Class<?> iface) {
		return (T) this;
	}

	@Override
	public IPrefixSelect base(String iri) {
		list.add(new Base(iri));
		return compose(IPrefixSelect.class);
	}

	@Override
	public IPrefixSelect prefix(String prefix, String iri) {
		list.add(new Prefix(prefix, iri));
		return compose(IPrefixSelect.class);
	}

	@Override
	public IGroupingHavingOrderOffsetLimitGet selectReduced(String projection,
			where where) {
		list.add(new Select(SelectType.REDUCED, projection, where));
		return compose(IGet.class);
	}

	@Override
	public IGroupingHavingOrderOffsetLimitGet selectDistinct(String projection,
			where where) {
		list.add(new Select(SelectType.DISTINCT, projection, where));
		return compose(IGet.class);
	}

	@Override
	public IGroupingHavingOrderOffsetLimitGet select(String projection,
			where where) {
		list.add(new Select(SelectType.DEFAULT, projection, where));
		return compose(IGet.class);
	}

	@Override
	public IFromGroupingHavingOrderOffsetLimitGet selectReduced(
			String projection) {
		return (QueryComposite) selectReduced(projection, null);
	}

	@Override
	public IFromGroupingHavingOrderOffsetLimitGet selectDistinct(
			String projection) {
		return (QueryComposite) selectDistinct(projection, null);
	}

	@Override
	public IFromGroupingHavingOrderOffsetLimitGet select(String projection) {
		return (QueryComposite) select(projection, null);
	}

	@Override
	public IFromGroupingHavingOrderOffsetLimitGet from(String iri) {
		return (QueryComposite) from(iri, null);
	}

	@Override
	public IFromGroupingHavingOrderOffsetLimitGet fromNamed(String iri) {
		return (QueryComposite) fromNamed(iri, null);
	}

	@Override
	public IGroupingHavingOrderOffsetLimitGet from(String iri, where where) {
		list.add(new From(FromType.DEFAULT, iri, where));
		return compose(IGroupingHavingOrderOffsetLimitGet.class);
	}

	@Override
	public IGroupingHavingOrderOffsetLimitGet fromNamed(String iri, where where) {
		list.add(new From(FromType.NAMED, iri, where));
		return compose(IGroupingHavingOrderOffsetLimitGet.class);
	}

	@Override
	public IHavingOrderOffsetLimitGet groupBy(String expression) {
		list.add(new Expression(ExpressionType.GROUP_BY, expression));
		return compose(IHavingOrderOffsetLimitGet.class);
	}

	@Override
	public IOrderOffsetLimitGet having(String expression) {
		list.add(new Expression(ExpressionType.HAVING, expression));
		return compose(IOrderOffsetLimitGet.class);
	}

	@Override
	public IOffsetLimitGet orderBy(String expression) {
		list.add(new Expression(ExpressionType.ORDER_BY_ASC, expression));
		return compose(IOrderOffsetLimitGet.class);
	}

	@Override
	public IOffsetLimitGet orderByDescending(String expression) {
		list.add(new Expression(ExpressionType.ORDER_BY_DESC, expression));
		return compose(IOrderOffsetLimitGet.class);
	}

	@Override
	public IGet limit(int limit) {
		list.add(new IntegerExpression(IntegerExpressionType.LIMIT, limit));
		return compose(IGet.class);
	}

	@Override
	public ILimitGet offset(int offset) {
		list.add(new IntegerExpression(IntegerExpressionType.OFFSET, offset));
		return compose(ILimitGet.class);
	}

}
