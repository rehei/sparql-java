package org.bitbucket.rehei.sparqljava.composite;

public interface IHavingOrderOffsetLimitGet extends IHaving, IOrder, IOffset,
		ILimit, IGet {

}
