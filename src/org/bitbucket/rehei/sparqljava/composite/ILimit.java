package org.bitbucket.rehei.sparqljava.composite;

public interface ILimit {

	public IGet limit(int limit);
}
