package org.bitbucket.rehei.sparqljava.composite;

public interface IOrderOffsetLimitGet extends IOrder, IOffset, ILimit, IGet {

}
