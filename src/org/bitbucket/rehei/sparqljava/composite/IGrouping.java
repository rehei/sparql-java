package org.bitbucket.rehei.sparqljava.composite;

public interface IGrouping {

	public IHavingOrderOffsetLimitGet groupBy(String str);
	
}
