package org.bitbucket.rehei.sparqljava.composite;

import org.bitbucket.rehei.sparqljava.query.where;

public interface ISelect {

	public IGroupingHavingOrderOffsetLimitGet selectReduced(String projection, where where);

	public IGroupingHavingOrderOffsetLimitGet selectDistinct(String projection, where where);

	public IGroupingHavingOrderOffsetLimitGet select(String projection, where where);

	public IFromGroupingHavingOrderOffsetLimitGet selectReduced(String projection);

	public IFromGroupingHavingOrderOffsetLimitGet selectDistinct(String projection);

	public IFromGroupingHavingOrderOffsetLimitGet select(String projection);

}