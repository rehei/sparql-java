package org.bitbucket.rehei.sparqljava.composite;

public interface IOrder {

	public IOffsetLimitGet orderBy(String str);
	
	public IOffsetLimitGet orderByDescending(String str);
	
}
