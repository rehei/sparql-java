package org.bitbucket.rehei.sparqljava.composite;

public interface IOffset {

	public ILimitGet offset(int offset);

}
