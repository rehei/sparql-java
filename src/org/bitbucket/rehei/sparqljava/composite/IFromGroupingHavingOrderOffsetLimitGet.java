package org.bitbucket.rehei.sparqljava.composite;

public interface IFromGroupingHavingOrderOffsetLimitGet extends IFrom, IGrouping,
		IHaving, IOrder, IOffset, ILimit, IGet {

}
