package org.bitbucket.rehei.sparqljava.query;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.rehei.sparqljava.expression.IVisitor;
import org.bitbucket.rehei.sparqljava.model.GraphExpression;
import org.bitbucket.rehei.sparqljava.model.IVisitable;
import org.bitbucket.rehei.sparqljava.model.SimpleExpression;
import org.bitbucket.rehei.sparqljava.model.SubSelect;
import org.bitbucket.rehei.sparqljava.model.UnionExpression;
import org.bitbucket.rehei.sparqljava.model.GraphExpression.GraphExpressionType;
import org.bitbucket.rehei.sparqljava.model.Select.SelectType;

public class group implements IVisitable {

	public List<IVisitable> expressions = new ArrayList<IVisitable>();

	public UnionExpression $(group group) {
		UnionExpression tmp = new UnionExpression(group);
		expressions.add(tmp);
		return tmp;
	}

	public void $(String expression) {
		expressions.add(new SimpleExpression(expression));
	}

	public void optional(String expression) {
		expressions.add(new GraphExpression(GraphExpressionType.OPTIONAL,
				expression));
	}

	public void filter(String expression) {
		expressions.add(new GraphExpression(GraphExpressionType.FILTER,
				expression));
	}

	public void selectReduced(String projection, where where) {
		expressions.add(new SubSelect(SelectType.REDUCED, projection, where));
	}

	public void selectDistinct(String projection, where where) {
		expressions.add(new SubSelect(SelectType.DISTINCT, projection, where));
	}

	public void select(String projection, where where) {
		expressions.add(new SubSelect(SelectType.DEFAULT, projection, where));
	}

	@Override
	public <T> T accept(IVisitor<T> visitor) {
		return visitor.visit(this);
	}

}
