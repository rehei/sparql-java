package org.bitbucket.rehei.sparqljava.query;

import org.bitbucket.rehei.sparqljava.composite.IBasePrefixSelect;
import org.bitbucket.rehei.sparqljava.composite.IGet;
import org.bitbucket.rehei.sparqljava.composite.IPrefixSelect;
import org.bitbucket.rehei.sparqljava.composite.QueryComposite;

public class Q {

	public static IBasePrefixSelect create() {
		return new QueryComposite().compose(IBasePrefixSelect.class);
	}

	public static IPrefixSelect base(String url) {
		return create().base(url);
	}

	public static IPrefixSelect prefix(String prefix, String url) {
		return create().prefix(prefix, url);
	}

	public static IGet selectReduced(String projection, where where) {
		return create().selectReduced(projection, where);
	}

	public static IGet selectDistinct(String projection, where where) {
		return create().selectDistinct(projection, where);
	}

	public static IGet select(String projection, where where) {
		return create().select(projection, where);
	}

}
