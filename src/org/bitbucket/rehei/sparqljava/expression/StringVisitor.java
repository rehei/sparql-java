package org.bitbucket.rehei.sparqljava.expression;

import java.util.List;

import org.bitbucket.rehei.sparqljava.model.Base;
import org.bitbucket.rehei.sparqljava.model.Expression;
import org.bitbucket.rehei.sparqljava.model.From;
import org.bitbucket.rehei.sparqljava.model.GraphExpression;
import org.bitbucket.rehei.sparqljava.model.IVisitable;
import org.bitbucket.rehei.sparqljava.model.IntegerExpression;
import org.bitbucket.rehei.sparqljava.model.Prefix;
import org.bitbucket.rehei.sparqljava.model.Select;
import org.bitbucket.rehei.sparqljava.model.SimpleExpression;
import org.bitbucket.rehei.sparqljava.model.SubSelect;
import org.bitbucket.rehei.sparqljava.model.UnionExpression;
import org.bitbucket.rehei.sparqljava.model.From.FromType;
import org.bitbucket.rehei.sparqljava.model.Select.SelectType;
import org.bitbucket.rehei.sparqljava.query.group;

public class StringVisitor implements IVisitor<String> {

	private int depth = 0;

	private String tab() {
		String tmp = "";
		for (int i = 0; i < depth; i++) {
			tmp += "  ";
		}
		return tmp;
	}

	@Override
	public String visit(List<IVisitable> list) {
		String expr = "";
		for (IVisitable visitable : list) {
			expr += tab() + visitable.accept(this) + "\n";
		}
		return expr;
	}

	@Override
	public String visit(Base base) {
		return String.format("BASE <%s>", base.iri);
	}

	@Override
	public String visit(Expression expression) {
		String prefix = expression.type.name().replace("_", " ");
		return String.format("%s { %s } ", prefix, expression.expression);
	}

	@Override
	public String visit(From from) {
		String prefix = "";
		if (!from.type.equals(FromType.DEFAULT)) {
			prefix = from.type.name().replace("_", " ") + " ";
		}
		if (from.where != null) {
			return String.format("FROM %s<%s>\nWHERE {\n%s}", prefix,
					from.iri, from.where.accept(this));
		}
		return String.format("FROM %s <%s>", prefix, from.iri);
	}

	@Override
	public String visit(IntegerExpression integerExpression) {
		String prefix = integerExpression.type.name().replace("_", " ");
		return String.format("%s %d ", prefix, integerExpression.param);
	}

	@Override
	public String visit(Prefix prefix) {
		return String.format("PREFIX %s: <%s>", prefix.prefix, prefix.iri);
	}

	@Override
	public String visit(Select select) {
		return visitSelect(select);
	}

	private String visitSelect(Select select) {
		String prefix = "";
		if (!select.type.equals(SelectType.DEFAULT)) {
			prefix = select.type.name() + " ";
		}
		if (select.where != null) {
			return tab() + String.format("SELECT %s%s WHERE {\n%s%s}", prefix,
					select.projection, select.where.accept(this), tab());
		}
		return String.format("SELECT %s %s ", prefix, select.projection);
	}
	
	@Override
	public String visit(SubSelect select) {
		depth++;
		String tmp = visitSelect(select);
		depth--; 
		String out = "";
		out += "{\n";
		out += tmp;
		out += "\n";
		out += tab() + "}";
		return out;
	}


	@Override
	public String visit(group group) {
		depth++;
		String tmp = this.visit(group.expressions);
		depth--;
		return tmp;
	}

	@Override
	public String visit(GraphExpression graphExpression) {
		return String.format("%s { %s }", graphExpression.type.name(),
				graphExpression.expression);
	}

	@Override
	public String visit(SimpleExpression simpleExpression) {
		return simpleExpression.expression + " .";
	}

	@Override
	public String visit(UnionExpression unionExpression) {
		String tmp = ""; 
		boolean first = true;
		for(group group : unionExpression.groups) {
			if(first) {
				first = false;
			} else {
				tmp+= " UNION ";
			}
			tmp += "{\n";
			depth++;
			tmp += this.visit(group);
			depth--;
			tmp += tab() + "}";
		}
		return tmp;
	}

}
