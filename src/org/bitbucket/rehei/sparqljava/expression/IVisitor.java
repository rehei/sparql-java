package org.bitbucket.rehei.sparqljava.expression;

import java.util.List;

import org.bitbucket.rehei.sparqljava.model.Base;
import org.bitbucket.rehei.sparqljava.model.Expression;
import org.bitbucket.rehei.sparqljava.model.From;
import org.bitbucket.rehei.sparqljava.model.GraphExpression;
import org.bitbucket.rehei.sparqljava.model.IVisitable;
import org.bitbucket.rehei.sparqljava.model.IntegerExpression;
import org.bitbucket.rehei.sparqljava.model.Prefix;
import org.bitbucket.rehei.sparqljava.model.Select;
import org.bitbucket.rehei.sparqljava.model.SimpleExpression;
import org.bitbucket.rehei.sparqljava.model.SubSelect;
import org.bitbucket.rehei.sparqljava.model.UnionExpression;
import org.bitbucket.rehei.sparqljava.query.group;

public interface IVisitor<T> {

	public T visit(List<IVisitable> list);
	
	public T visit(Base base);
	
	public T visit(Expression expression);

	public T visit(From from);

	public T visit(IntegerExpression integerExpression);

	public T visit(Prefix prefix);

	public T visit(Select select);
	
	public T visit(SubSelect select);

	public T visit(group group);

	public T visit(GraphExpression graphExpression);

	public T visit(SimpleExpression simpleExpression);

	public T visit(UnionExpression unionExpression);

}
